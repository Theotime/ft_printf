#ifndef FT_PRINTF
# define FT_PRINTF

# include "libft.h"
# include <stdarg.h>

int			ft_printf(const char *restrict format, ...);
char		*ft_sprintf(const char *restrict format, ...);

char		*ft_vprintf(const char *restrict format, va_list *av);

#endif