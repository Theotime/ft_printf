#include "ft_printf.h"
#include <stdio.h>

static int		ft_printf_extract(va_list *av, char *format, char **res)
{
	int		n;
	char	*tmp;

	n = 1;
	tmp = NULL;
	// while (format[n] != 's' && format[n] != 'd')
	// 	++n;
	if (format[n] == 's')
		tmp = ft_strdup(va_arg(*av, char*));
	else if (format[n] == 'd')
		tmp = ft_itoa(va_arg(*av, int));
	else if (format[n] == '%')
		tmp = ft_strdup("%");
	else if (format[n] == 'c')
	{
		tmp = ft_strnew(2);
		tmp[0] = va_arg(*av, int);
	}
	++n;
	*res = tmp;
	return n;
}

char			*ft_vprintf(const char *restrict format, va_list *av)
{
	char		*result;
	char		*tmp;
	int			n;
	int			old_pos;

	result = ft_strnew(0);
	n = 0;
	old_pos = 0;
	while (format[old_pos + n])
	{
		if (format[old_pos + n] == '%')
		{
			result = ft_strnjoin(result, (char*)format, old_pos, n);
			old_pos += n + ft_printf_extract(av, \
				(char*)(format + old_pos + n), &tmp);
			n = 0;
			result = ft_strnjoin(result, tmp, 0, ft_strlen(tmp));
			free(tmp);
		}
		++n;
	}
	if (n > 0)
		result = ft_strnjoin(result, (char*)format, old_pos, n);
	return (result);
}

int				ft_printf(const char *restrict format, ...)
{
	va_list		av;
	char		*str;

	va_start(av, format);
	str = ft_vprintf(format, &av);
	va_end(av);
	if(*str)
		ft_putstr(str);
	else
		return (0);
	return (1);
}