#include "ft_printf.h"
#include <stdio.h>


int			main(int ac, char **av)
{
	(void)ac;
	(void)av;
	// ft_printf("Toto as %d ans et il %s, c'est cool non ?\n", 10, "aime les carrotes");
	ft_printf("ft_printf\n	String : %s\n	Int : %d\n	Char : %c\n", "Toto aime les carrotes", 10, 'y');
	printf("printf\n	String : %s\n	Int : %d\n	Char : %c\n", "Toto aime les carrotes", 10, 'y');

	printf("%.2f\n", 10.555);
	printf("%.5d\n", 10);

	// printf("%s\n", vprintf(av[1], av[1], *((va_list*)&av[2])));
	return (0);
}
